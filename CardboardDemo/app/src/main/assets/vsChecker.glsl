// Vertex Shader
precision highp float;

attribute vec4 vertex_in;
attribute vec3 normal_in;
attribute vec2 uv_in;

uniform mat4 pvm;
uniform mat4 vm;

uniform vec4 lightpos;

varying vec3 v_normal;
varying vec3 v_lightdir;
varying vec3 v_viewdir;
varying vec2 v_uv;

void main()
{
    gl_Position = pvm * vertex_in;

    vec4 h = vm * vertex_in;
    vec3 mvPos = h.xyz / h.w;

    v_lightdir = lightpos.xyz - mvPos;
    v_viewdir = -mvPos;

    v_normal = mat3(vm) * normal_in;

    v_uv = uv_in;
}