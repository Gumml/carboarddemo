// Fragment Shader
precision highp float;

varying vec3 normal_vary;
varying vec3 lightdir_vary;
varying vec3 viewdir_vary;

struct lighting
{
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
};

struct material
{
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    vec4 emissive;
    float shininess;
};

uniform lighting light;
uniform material mat;


void main()
{
    vec3 nNormal = normalize(normal_vary);
    vec3 nLightdir = normalize(lightdir_vary);
    vec3 nViewdir = normalize(viewdir_vary);

    float d = max(0.0, dot(nNormal, nLightdir));
    vec4 lightcolor = (light.ambient * mat.ambient) + (light.diffuse * mat.diffuse * d);

    //    Only compute expensive spec light when visible at all
    float s = 0.0;
    if(d > 0.0)
    {
        vec3 ref = normalize(reflect(-nLightdir, nNormal));
        s = pow(max(0.0, dot(nViewdir, ref)), mat.shininess);
    }

    gl_FragColor = lightcolor + (s * light.specular * mat.specular) + mat.emissive;
}