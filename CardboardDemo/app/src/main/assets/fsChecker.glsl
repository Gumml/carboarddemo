// Fragment Shader
precision highp float;

varying vec3 v_normal;
varying vec3 v_lightdir;
varying vec3 v_viewdir;
varying vec2 v_uv;

uniform float gridsize;


bool grid(in float val, in float e);


void main()
{
    vec3 nNormal = normalize(v_normal);
    vec3 nLightdir = normalize(v_lightdir);
    vec3 nViewdir = normalize(v_viewdir);

    vec2 color = v_uv;

    float d = max(0.0, dot(nNormal, nLightdir));
    vec4 lightcolor = vec4(0.5) + vec4(1.0) * d;

	vec4 gridcolor = vec4(0);

	if(grid(v_uv.x, gridsize))
		gridcolor = vec4(1);

	if(grid(v_uv.y, gridsize))
		gridcolor = vec4(1);

    gl_FragColor = lightcolor * vec4(color.x, color.y, color.x + color.y / length(color), 1) + gridcolor;
}


bool grid(in float val, in float e)
{
	if(abs(val - 0.1) < e)
		return true;

	else if(abs(val - 0.2) < e)
		return true;

	else if(abs(val - 0.3) < e)
		return true;

	else if(abs(val - 0.4) < e)
		return true;

	else if(abs(val - 0.5) < e)
		return true;

	else if(abs(val - 0.6) < e)
		return true;

	else if(abs(val - 0.7) < e)
		return true;

	else if(abs(val - 0.8) < e)
		return true;

	else if(abs(val - 0.9) < e)
		return true;

	else if(abs(val - 1.0) < e)
		return true;

	else
		return false;

}