package edu.hm.cs.markusmiller.cardboarddemo;

/**
 * Created by Markus Miller on 21.04.2016.
 */
public interface InputEventDispatcher
{
    public void onForwardAxis(float direction);

    public void onRightAxis(float direction);

    public void onKeyA();

    public void onKeyB();

    public void onKeyX();

    public void onKeyY();

    public void onLeftTouch();

    public void onRightTouch();

    public int surfaceHeight();

    public int surfaceWidth();
}
