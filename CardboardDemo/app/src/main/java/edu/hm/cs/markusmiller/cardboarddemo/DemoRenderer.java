package edu.hm.cs.markusmiller.cardboarddemo;

import android.content.Context;

import static android.opengl.GLES20.*;

import android.opengl.Matrix;
import android.util.AttributeSet;
import android.util.Log;

import com.google.vr.sdk.base.Eye;
import com.google.vr.sdk.base.GvrView;
import com.google.vr.sdk.base.HeadTransform;
import com.google.vr.sdk.base.Viewport;
import com.google.vr.sdk.base.sensors.HeadTracker;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.egl.EGLConfig;

import edu.hm.cs.markusmiller.cardboarddemo.jglm.Mat3;
import edu.hm.cs.markusmiller.cardboarddemo.jglm.Vec3;

/**
 * Created by Markus Miller on 22.02.2016.
 */
public class DemoRenderer extends GvrView implements GvrView.StereoRenderer, InputEventDispatcher
{
    //    Variables
    //    ----------------------------------------------------------------------------------------------

    //    Keep a copy of the context
    private Context context;

    private Shaderprogram coloredShader;
    private Shaderprogram materialShader;
    private Shaderprogram groundShader;

    private FloatBuffer cubeVertices;
    private FloatBuffer cubeColors;
    private FloatBuffer cubeNormals;

	private FloatBuffer groundVertices;
	private FloatBuffer groundNormals;
	private FloatBuffer groundUVs;

    private DataStructures.Matrices matrices = new DataStructures.Matrices();

    private DataStructures.Locations coloredLocations = new DataStructures.Locations();
    private DataStructures.Locations materialLocations = new DataStructures.Locations();
    private DataStructures.Locations groundLocations = new DataStructures.Locations();

    private DataStructures.AnimationParameters animation = new DataStructures.AnimationParameters();

    private float[] lightpos = { 0.f, 5.f, 0.f, 1.f };
    private float[] lightpos_eye = new float[4];

    private DataStructures.LightParameters light = new DataStructures.LightParameters();
	private DataStructures.MaterialParameters material = new DataStructures.MaterialParameters();

	private Camera cam;
	private MovementState movementstate;
	private Vec3 movementDirection;
	private Mat3 currentHeadRotation;
	private final float speed = 0.1f;
	private final Vec3 zero = new Vec3(0.f, 0.f, 0.f);
    //    ----------------------------------------------------------------------------------------------


    //    Private Methods
    //    ----------------------------------------------------------------------------------------------
    private void init(Context c)
    {
        context = c;

//        setRestoreGLStateEnabled(false);
        setRenderer(this);

	    cam = new Camera();
	    cam.moveTo(0.f, 2.f, 0.f);
	    cam.setTarget(0.f, 2.f, -1.f);

	    movementstate = MovementState.Stop;
	    movementDirection = zero;
    }


    static private void checkGL(String lbl)
    {
        int error;
        while ((error = glGetError()) != GL_NO_ERROR)
        {
            Log.e("DemoRenderer", lbl + ": Error " + error);
            throw new RuntimeException(lbl + ": Error " + error);
        }
    }

	private void calculateMovementDirection()
	{
		Vec3 forward = new Vec3(0.f, 0.f, -1.f);

		Vec3 direction = currentHeadRotation.multiply(forward);
		direction = direction.multiply(1.f / direction.getLength());
		direction = direction.multiply(movementstate.getDirection());

		movementDirection = direction.multiply(speed);
	}
    //    ----------------------------------------------------------------------------------------------


    //    Constructors
    //    ----------------------------------------------------------------------------------------------
    public DemoRenderer(Context context)
    {
        super(context);

        init(context);
    }

    public DemoRenderer(Context context, AttributeSet attrs)
    {
        super(context, attrs);

        init(context);
    }
    //    ----------------------------------------------------------------------------------------------


    //    Public Methods
    //    ----------------------------------------------------------------------------------------------
    @Override
    public void onNewFrame(HeadTransform headTransform)
    {
        // Put all global scene stuff like model transformation, view transformation etc. here
        glClearColor(0.f, .6f, .8f, 1.f);
        glEnable(GL_DEPTH_TEST);

//        Matrix.setLookAtM(matrices.cam, 0, 0.f, 0.f, 0.f, 0.f, 0.f, -1.f, 0.f, 1.f, 0.f);

	    matrices.cam = cam.getViewMatrix();

//	    cam.moveForward(0.01f);
//	    cam.moveAlongAxis(-0.01f, 0.f, 0.01f);
	    cam.moveAlongAxis(movementDirection.getX(), movementDirection.getY(), movementDirection.getZ());

        headTransform.getHeadView(matrices.headView, 0);

	    currentHeadRotation = new Mat3(matrices.headView[0], matrices.headView[4], matrices.headView[8], matrices.headView[1], matrices.headView[5], matrices.headView[9], matrices.headView[2], matrices.headView[6], matrices.headView[10]);
    }


	@Override
	public void onFinishFrame(Viewport viewport)
	{

	}

	@Override
    public void onDrawEye(Eye eye)
    {
        // Put the observer specific stuff here, so basically everything required to render the scene for one eye
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        Matrix.setIdentityM(matrices.view, 0);
        Matrix.multiplyMM(matrices.view, 0, eye.getEyeView(), 0, matrices.cam, 0);

        Matrix.multiplyMV(lightpos_eye, 0, matrices.view, 0, lightpos, 0);

        matrices.projection = eye.getPerspective(1.f, 100.f);

	    // Colored Cube
        Matrix.setIdentityM(matrices.model, 0);
        Matrix.translateM(matrices.model, 0, 2.f, 0.f, -7.5f);
        Matrix.rotateM(matrices.model, 0, animation.rotationAngleX, 1.f, 0.f, 0.f);
        Matrix.rotateM(matrices.model, 0, animation.rotationAngleY, 0.f, 1.f, 0.f);

        Matrix.multiplyMM(matrices.vm, 0, matrices.view, 0, matrices.model, 0);
        Matrix.multiplyMM(matrices.pvm, 0, matrices.projection, 0, matrices.vm, 0);


        coloredShader.use();

        glUniformMatrix4fv(coloredLocations.pvm, 1, false, matrices.pvm, 0);
        glUniformMatrix4fv(coloredLocations.vm, 1, false, matrices.vm, 0);

        glUniform4fv(coloredLocations.lightpos, 1, lightpos_eye, 0);

        glUniform4fv(coloredLocations.light_ambient, 1, light.ambient, 0);
        glUniform4fv(coloredLocations.light_diffuse, 1, light.diffuse, 0);
        glUniform4fv(coloredLocations.light_specular, 1, light.specular, 0);

        glVertexAttribPointer(coloredLocations.vertex_in, 3, GL_FLOAT, false, 0, cubeVertices);
        glEnableVertexAttribArray(coloredLocations.vertex_in);

        glVertexAttribPointer(coloredLocations.color_in, 4, GL_FLOAT, false, 0, cubeColors);
        glEnableVertexAttribArray(coloredLocations.color_in);

        glVertexAttribPointer(coloredLocations.normal_in, 3, GL_FLOAT, false, 0, cubeNormals);
        glEnableVertexAttribArray(coloredLocations.normal_in);

        glDrawArrays(GL_TRIANGLES, 0, 36);



	    // Material Cube
	    Matrix.setIdentityM(matrices.model, 0);
	    Matrix.translateM(matrices.model, 0, -2.f, 0.f, -7.5f);
	    Matrix.rotateM(matrices.model, 0, animation.rotationAngleX, 1.f, 0.f, 0.f);
	    Matrix.rotateM(matrices.model, 0, animation.rotationAngleY, 0.f, 1.f, 0.f);

	    Matrix.multiplyMM(matrices.vm, 0, matrices.view, 0, matrices.model, 0);
	    Matrix.multiplyMM(matrices.pvm, 0, matrices.projection, 0, matrices.vm, 0);


	    materialShader.use();

	    glUniformMatrix4fv(materialLocations.pvm, 1, false, matrices.pvm, 0);
	    glUniformMatrix4fv(materialLocations.vm, 1, false, matrices.vm, 0);

	    glUniform4fv(materialLocations.lightpos, 1, lightpos_eye, 0);

	    glUniform4fv(materialLocations.light_ambient, 1, light.ambient, 0);
	    glUniform4fv(materialLocations.light_diffuse, 1, light.diffuse, 0);
	    glUniform4fv(materialLocations.light_specular, 1, light.specular, 0);

	    glUniform4fv(materialLocations.mat_ambient, 1, material.ambient, 0);
	    glUniform4fv(materialLocations.mat_diffuse, 1, material.diffuse, 0);
	    glUniform4fv(materialLocations.mat_specular, 1, material.specular, 0);
	    glUniform4fv(materialLocations.mat_emissive, 1, material.emissive, 0);
	    glUniform1f(materialLocations.mat_shininess, material.shininess);

	    glVertexAttribPointer(materialLocations.vertex_in, 3, GL_FLOAT, false, 0, cubeVertices);
	    glEnableVertexAttribArray(materialLocations.vertex_in);

	    glVertexAttribPointer(materialLocations.normal_in, 3, GL_FLOAT, false, 0, cubeNormals);
	    glEnableVertexAttribArray(materialLocations.normal_in);

	    glDrawArrays(GL_TRIANGLES, 0, 36);



	    // Ground
	    Matrix.setIdentityM(matrices.model, 0);
	    Matrix.translateM(matrices.model, 0, 0.f, -2.f, -7.5f);
	    Matrix.rotateM(matrices.model, 0, animation.rotationAngleX, 1.f, 0.f, 0.f);
	    Matrix.rotateM(matrices.model, 0, animation.rotationAngleY, 0.f, 1.f, 0.f);

	    Matrix.multiplyMM(matrices.vm, 0, matrices.view, 0, matrices.model, 0);
	    Matrix.multiplyMM(matrices.pvm, 0, matrices.projection, 0, matrices.vm, 0);


	    groundShader.use();

	    glUniformMatrix4fv(groundLocations.pvm, 1, false, matrices.pvm, 0);
	    glUniformMatrix4fv(groundLocations.vm, 1, false, matrices.vm, 0);

	    glUniform4fv(groundLocations.lightpos, 1, lightpos_eye, 0);
	    glUniform1f(groundLocations.gridsize, 0.001f);

	    glVertexAttribPointer(groundLocations.vertex_in, 3, GL_FLOAT, false, 0, groundVertices);
	    glEnableVertexAttribArray(groundLocations.vertex_in);

	    glVertexAttribPointer(groundLocations.normal_in, 3, GL_FLOAT, false, 0, groundNormals);
	    glEnableVertexAttribArray(groundLocations.normal_in);

	    glVertexAttribPointer(groundLocations.uv_in, 2, GL_FLOAT, false, 0, groundUVs);
	    glEnableVertexAttribArray(groundLocations.uv_in);

	    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    }


	@Override
    public void onSurfaceChanged(int width, int height)
    {
        // Screen orientation or screen resolution has changed
        Log.i("DemoRenderer", "onSurfaceChanged");
    }

    @Override
    public void onSurfaceCreated(EGLConfig eglConfig)
    {
        // Initialize your rendering stuff here. Bug: glClearColor has no effect when called here

        coloredShader = new Shaderprogram(context, "vs.glsl", "fs.glsl");
        coloredLocations.vertex_in = coloredShader.getAttributeLocation("vertex_in");
        coloredLocations.color_in = coloredShader.getAttributeLocation("color_in");
        coloredLocations.normal_in = coloredShader.getAttributeLocation("normal_in");
        coloredLocations.pvm = coloredShader.getUniformLocation("pvm");
        coloredLocations.vm = coloredShader.getUniformLocation("vm");
        coloredLocations.lightpos = coloredShader.getUniformLocation("lightpos");
        coloredLocations.light_ambient = coloredShader.getUniformLocation("light.ambient");
        coloredLocations.light_diffuse = coloredShader.getUniformLocation("light.diffuse");
        coloredLocations.light_specular = coloredShader.getUniformLocation("light.specular");

        Log.i("DemoRenderer", "coloredShader --- v_in: " + coloredLocations.vertex_in + " c_in: " + coloredLocations.color_in + " n_in: " + coloredLocations.normal_in + " pvm: " + coloredLocations.pvm + " lp: " + coloredLocations.lightpos);


	    materialShader = new Shaderprogram(context, "vsMaterial.glsl", "fsMaterial.glsl");
	    materialLocations.vertex_in = materialShader.getAttributeLocation("vertex_in");
	    materialLocations.normal_in = materialShader.getAttributeLocation("normal_in");
	    materialLocations.pvm = materialShader.getUniformLocation("pvm");
	    materialLocations.vm = materialShader.getUniformLocation("vm");
	    materialLocations.lightpos = materialShader.getUniformLocation("lightpos");
	    materialLocations.light_ambient = materialShader.getUniformLocation("light.ambient");
	    materialLocations.light_diffuse = materialShader.getUniformLocation("light.diffuse");
	    materialLocations.light_specular = materialShader.getUniformLocation("light.specular");
	    materialLocations.mat_ambient = materialShader.getUniformLocation("mat.ambient");
	    materialLocations.mat_diffuse = materialShader.getUniformLocation("mat.diffuse");
	    materialLocations.mat_specular = materialShader.getUniformLocation("mat.specular");
	    materialLocations.mat_emissive = materialShader.getUniformLocation("mat.emissive");
	    materialLocations.mat_shininess = materialShader.getUniformLocation("mat.shininess");

	    material.diffuse = new float[] { .2f, .8f, .4f, 1.f };

	    Log.i("DemoRenderer", "materialShader --- v_in: " + materialLocations.vertex_in + " n_in: " + materialLocations.normal_in + " pvm: " + materialLocations.pvm + " lp: " + materialLocations.lightpos);


	    groundShader = new Shaderprogram(context, "vsChecker.glsl", "fsChecker.glsl");
	    groundLocations.vertex_in = groundShader.getAttributeLocation("vertex_in");
	    groundLocations.normal_in = groundShader.getAttributeLocation("normal_in");
	    groundLocations.uv_in = groundShader.getAttributeLocation("uv_in");
	    groundLocations.pvm = groundShader.getUniformLocation("pvm");
	    groundLocations.vm = groundShader.getUniformLocation("vm");
	    groundLocations.lightpos = groundShader.getUniformLocation("lightpos");
	    groundLocations.gridsize = groundShader.getUniformLocation("gridsize");

	    Log.i("DemoRenderer", "groundShader --- v_in: " + groundLocations.vertex_in + " n_in: " + groundLocations.normal_in + " uv_in: " + groundLocations.uv_in + " pvm: " + groundLocations.pvm + " lp: " + groundLocations.lightpos);


        cubeVertices = ByteBuffer.allocateDirect(GeometryData.Cube_Coordinates.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        cubeVertices.put(GeometryData.Cube_Coordinates);
        cubeVertices.position(0);

        cubeColors = ByteBuffer.allocateDirect(GeometryData.Cube_Colors.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        cubeColors.put(GeometryData.Cube_Colors);
        cubeColors.position(0);

        cubeNormals = ByteBuffer.allocateDirect(GeometryData.Cube_Normals.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        cubeNormals.put(GeometryData.Cube_Normals);
        cubeNormals.position(0);


	    groundVertices = ByteBuffer.allocateDirect(GeometryData.Ground_Vertices.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
	    groundVertices.put(GeometryData.Ground_Vertices);
	    groundVertices.position(0);

	    groundNormals = ByteBuffer.allocateDirect(GeometryData.Ground_Normals.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
	    groundNormals.put(GeometryData.Ground_Normals);
	    groundNormals.position(0);

	    groundUVs = ByteBuffer.allocateDirect(GeometryData.Ground_UVs.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
	    groundUVs.put(GeometryData.Ground_UVs);
	    groundUVs.position(0);

        Log.i("DemoRenderer", "onSurfaceCreated");
    }

    @Override
    public void onRendererShutdown()
    {
        // Free all GPU memory. Not guaranteed to be called, though

        coloredShader.delete();
        materialShader.delete();
        groundShader.delete();

        Log.i("DemoRenderer", "onRendererShutdown");
    }

//    public boolean onTouchEvent(MotionEvent e)
//    {
//
//        return false;
//    }

    public void onForwardAxis(float direction)
    {
        animation.rotationAngleX -= direction;
    }

    public void onRightAxis(float direction)
    {
        animation.rotationAngleY += direction;
    }

    public void onKeyA()
    {

    }

    public void onKeyB()
    {

    }

    public void onKeyX()
    {

    }

    public void onKeyY()
    {

    }

    public void onLeftTouch()
    {
        switch(movementstate)
        {
	        case Stop:
		        movementstate = MovementState.Move_Backward;
		        calculateMovementDirection();
		        break;
	        case Move_Forward:
		        movementstate = MovementState.Stop;
		        movementDirection = zero;
		        break;
	        case Move_Backward:
		        calculateMovementDirection();
		        break;
        }
    }

    public void onRightTouch()
    {
	    switch(movementstate)
	    {
		    case Stop:
			    movementstate = MovementState.Move_Forward;
			    calculateMovementDirection();
			    break;
		    case Move_Forward:
			    calculateMovementDirection();
			    break;
		    case Move_Backward:
			    movementstate = MovementState.Stop;
			    movementDirection = zero;
			    break;
	    }
    }

    public int surfaceHeight()
    {
        return getHeight();
    }

    public int surfaceWidth()
    {
        return getWidth();
    }
    //    ----------------------------------------------------------------------------------------------
}
