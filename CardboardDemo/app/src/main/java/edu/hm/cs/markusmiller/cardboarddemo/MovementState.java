package edu.hm.cs.markusmiller.cardboarddemo;

/**
 * Created by Markus Miller on 20.07.2016.
 */
public enum MovementState
{
	Move_Forward(1.f), Move_Backward(-1.f), Stop(0.f);

	private final float direction;

	MovementState(float direction)
	{
		this.direction = direction;
	}

	float getDirection()
	{
		return direction;
	}
}
