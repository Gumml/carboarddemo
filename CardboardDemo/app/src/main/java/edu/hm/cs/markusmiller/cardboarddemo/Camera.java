package edu.hm.cs.markusmiller.cardboarddemo;

import android.util.Log;

import edu.hm.cs.markusmiller.cardboarddemo.jglm.Mat3;
import edu.hm.cs.markusmiller.cardboarddemo.jglm.Mat4;
import edu.hm.cs.markusmiller.cardboarddemo.jglm.Vec3;


/**
 * Created by Markus Miller on 14.07.2016.
 */
public class Camera
{
	//    Constants
	//    ----------------------------------------------------------------------------------------------
	//    ----------------------------------------------------------------------------------------------



	//    Variables
	//    ----------------------------------------------------------------------------------------------
	private Vec3 position;
	private Vec3 target;
	private Vec3 up;
	float upOrientation = 0.f;
	//    ----------------------------------------------------------------------------------------------



	//    Concstructors
	//    ----------------------------------------------------------------------------------------------
	public Camera()
	{
		this(new Vec3(0.f, 0.f, 0.f), new Vec3(0.f, 0.f, -1.f), new Vec3(0.f, 1.f, 0.f));
	}

	public Camera(Vec3 position, Vec3 target, Vec3 up)
	{
		Vec3 p, t, u;

		p = position;
		t = target;
		u = up;
		init(p, t, u);
	}
	//    ----------------------------------------------------------------------------------------------



	//    Private Methods
	//    ----------------------------------------------------------------------------------------------
	private void init(Vec3 position, Vec3 target, Vec3 up)
	{
		this.position = position;
		this.target = target;
		this.up = up;
	}


	private void adjustUp()
	{
		Vec3 left;
		Vec3 yVec = new Vec3(0.f, 1.f, 0.f);

		Vec3 delta = target.subtract(position);
		delta = delta.multiply(1.f / delta.getLength());

		if(delta.equals(yVec))
			up = new Vec3(0.f, 0.f, 1.f);

		else if(delta.equals(yVec.getNegated()))
			up = new Vec3(0.f, 0.f, -1.f);

		else
		{
			left = yVec.cross(delta);
			left = left.multiply(1.f / left.getLength());

			up = delta.cross(left);
			up = up.multiply(1.f / up.getLength());
		}

		if(upOrientation != 0.f)
		{
			Vec3 axis = target.subtract(position);
			axis = axis.multiply(1.f / axis.getLength());

			float c = (float) Math.cos(upOrientation);
			float s = (float) Math.sin(upOrientation);

			float one_c = 1.f - c;

			float x = axis.getX(), y = axis.getY(), z = axis.getZ();

			float yx = y * x;
			float zx = z * x;
			float zy = z * y;

			float a11 = x * x * one_c + c;      float a12 = yx * one_c - z * s;     float a13 = zx * one_c + y * s;
			float a21 = yx * one_c + z * s;     float a22 = y * y * one_c + c;      float a23 = zy * one_c - x * s;
			float a31 = zx * one_c - y * s;     float a32 = zy * one_c + x * s;     float a33 = z * z * one_c + c;

			Mat3 rotation = new Mat3(a11, a12, a13, a21, a22, a23, a31, a32, a33);
			up = rotation.multiply(up);
		}
	}
	//    ----------------------------------------------------------------------------------------------



	// Public Methods
	//    ----------------------------------------------------------------------------------------------
	public void moveTo(Vec3 newPosition)
	{
		position = newPosition;
	}


	public void moveTo(float x, float y, float z)
	{
		position = new Vec3(x, y, z);
	}


	public void moveForward(float delta)
	{
		Vec3 forward = target.subtract(position);
		forward = forward.multiply(1.f / forward.getLength());

		Vec3 movevector = position.add(forward.multiply(delta));
		moveTo(movevector);
		target = target.add(forward.multiply((delta)));
	}


	public void moveAlongAxis(float x, float y, float z)
	{
		Vec3 forward = target.subtract(position);
		forward = forward.multiply((1.f / forward.getLength()));

		Vec3 f = new Vec3(x, y, z);
		position = position.add(f);
		target = target.add(f);
	}


	public void setTarget(Vec3 newTarget)
	{
		target = newTarget;
	}


	public void setTarget(float x, float y, float z)
	{
		target = new Vec3(x, y, z);
	}


	public void setViewDirection(Vec3 viewDirection)
	{
		target = position.add(viewDirection);
	}


	public void setViewDirection(float x, float y, float z)
	{
		target = position.add(new Vec3(x, y, z));
	}


	public float[] getViewMatrix()
	{
		Vec3 delta = target.subtract(position);
		delta = delta.multiply(1.f / delta.getLength());

		adjustUp();

		Vec3 s = delta.cross(up);
		s = s.multiply(1.f / s.getLength());

		Vec3 u = s.cross(delta);

		Mat4 m = new Mat4(s.getX(), u.getX(), -delta.getX(), 0.f, s.getY(), u.getY(), -delta.getY(), 0.f, s.getZ(), u.getZ(), -delta.getZ(), 0.f, 0.f, 0.f, 0.f, 1.f);

		m = m.multiply(new Mat4(1.f, 0.f, 0.f, 0.f, 0.f, 1.f, 0.f, 0.f, 0.f, 0.f, 1.f, 0.f, -position.getX(), -position.getY(), -position.getZ(), 1.f));

		return m.getBuffer().array();
	}


	public void setUpOrientation(float orientation)
	{
		upOrientation = orientation;
	}


	public void reset()
	{
		init(new Vec3(0.f, 0.f, 0.f), new Vec3(0.f, 0.f, -1.f), new Vec3(0.f, 1.f, 0.f));
	}


	public Vec3 getPosition()
	{
		return position;
	}


	public Vec3 getForward()
	{
		Vec3 forward = target.subtract(position);
		forward = forward.multiply(1.f / forward.getLength());

		return forward;
	}
	//    ----------------------------------------------------------------------------------------------
}
