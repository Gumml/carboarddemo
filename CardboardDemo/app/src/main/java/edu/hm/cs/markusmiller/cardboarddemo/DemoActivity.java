package edu.hm.cs.markusmiller.cardboarddemo;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;

import com.google.vr.sdk.base.GvrActivity;

public class DemoActivity extends GvrActivity
{
    private CardboardDemoInputHandler input;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo);

        DemoRenderer renderer = (DemoRenderer) findViewById(R.id.demorenderer);
        setGvrView(renderer);

        input = new CardboardDemoInputHandler(renderer);
    }


    @Override
    public boolean dispatchGenericMotionEvent(MotionEvent ev)
    {
        input.onInput(ev);
        return super.dispatchGenericMotionEvent(ev);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event)
    {
        input.onInput(event);
        if (event.getKeyCode() != KeyEvent.KEYCODE_BACK)
            return true;
        return super.dispatchKeyEvent(event);
    }

    public boolean onTouchEvent(MotionEvent e)
    {
	    return input.onTouchEvent(e) && super.onTouchEvent(e);
    }
}
