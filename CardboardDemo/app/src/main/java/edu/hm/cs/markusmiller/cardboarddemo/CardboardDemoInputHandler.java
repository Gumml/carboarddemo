package edu.hm.cs.markusmiller.cardboarddemo;

import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;

/**
 * Created by Markus Miller on 21.04.2016.
 */
public class CardboardDemoInputHandler
{
    //    Variables
    //    ----------------------------------------------------------------------------------------------
    private InputEventDispatcher dispatcher;

    private float x = 0.f;

    private float y = 0.f;
    //    ----------------------------------------------------------------------------------------------

    //    Constructors
    //    ----------------------------------------------------------------------------------------------
    public CardboardDemoInputHandler(InputEventDispatcher d)
    {
        dispatcher = d;
    }
    //    ----------------------------------------------------------------------------------------------


    //    Public Methods
    //    ----------------------------------------------------------------------------------------------
    public void onInput(MotionEvent e)
    {
        //        Log.i("CardboardDemo", e.toString());
        x = e.getX();
        y = -1.f * e.getY();
    }

    public void onInput(KeyEvent e)
    {
        if (e.getAction() == KeyEvent.ACTION_DOWN)
        {
            switch (e.getKeyCode())
            {
                case KeyEvent.KEYCODE_BUTTON_A:
                    // Fake touch
                    dispatcher.onLeftTouch();
                    dispatcher.onKeyA();
                    Log.i("CardboardDemo", "Button A!");
                    break;
                case KeyEvent.KEYCODE_BUTTON_B:
                    // Fake touch
                    dispatcher.onRightTouch();
                    dispatcher.onKeyB();
                    Log.i("CardboardDemo", "Button B!");
                    break;
                case KeyEvent.KEYCODE_BUTTON_X:
                    dispatcher.onKeyX();
                    Log.i("CardboardDemo", "Button X!");
                    break;
                case KeyEvent.KEYCODE_BUTTON_Y:
                    dispatcher.onKeyY();
                    Log.i("CardboardDemo", "Button Y!");
                    break;
                case KeyEvent.KEYCODE_DPAD_RIGHT:
                case KeyEvent.KEYCODE_DPAD_LEFT:
                    dispatcher.onRightAxis(x);
                    Log.i("CardboardDemo", "X: " + x);
                    break;
                case KeyEvent.KEYCODE_DPAD_UP:
                case KeyEvent.KEYCODE_DPAD_DOWN:
                    dispatcher.onForwardAxis(y);
                    Log.i("CardboardDemo", "Y: " + y);
                    break;
            }
        }
        //        Log.i("CardboardDemo", e.toString());
    }

    public boolean onTouchEvent(MotionEvent e)
    {
        float separator = dispatcher.surfaceWidth() / 2.f;

        if(e.getAction() == MotionEvent.ACTION_UP)
        {
            if(e.getX() <= separator)
            {
                dispatcher.onLeftTouch();
                return true;
            }
            else if(e.getX() > separator)
            {
                dispatcher.onRightTouch();
                return true;
            }
            else
                return false;
        }
        else
            return false;
    }
    //    ----------------------------------------------------------------------------------------------
}
