package edu.hm.cs.markusmiller.cardboarddemo;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static android.opengl.GLES20.*;

/**
 * Created by Markus Miller on 24.02.2016.
 */
public class Shaderprogram
{
    //    Variables
    //    ----------------------------------------------------------------------------------------------
    private int program;
    //    ----------------------------------------------------------------------------------------------


    //    Private Methods
    //    ----------------------------------------------------------------------------------------------
    private String loadText(AssetManager assets, String path) throws IOException
    {
        String line;

        BufferedReader file = new BufferedReader(new InputStreamReader(assets.open(path)));

        StringBuilder str = new StringBuilder();

        line = file.readLine();

        while (line != null)
        {
            str.append(line);
            str.append("\n");
            line = file.readLine();
        }

        file.close();

        return str.toString();
    }


    private int createShader(int shadertype, String source)
    {
        int shader = glCreateShader(shadertype);

        if (shader != 0)
        {
            glShaderSource(shader, source);
            glCompileShader(shader);

            int status[] = new int[1];
            glGetShaderiv(shader, GL_COMPILE_STATUS, status, 0);

            if (status[0] == 0)
            {
                Log.e("Shaderprogram", "Could not compiler shader " + shadertype + ":");
                Log.e("Shaderprogram", glGetShaderInfoLog(shader));
                glDeleteShader(shader);
                shader = -1;
            }
        }

        return shader;
    }
    //    ----------------------------------------------------------------------------------------------


    //    Constructor
    //    ----------------------------------------------------------------------------------------------
    public Shaderprogram(Context c, String vsPath, String fsPath)
    {
        AssetManager assets = c.getAssets();

        String vs = null, fs = null;

        try
        {
            vs = loadText(assets, vsPath);
        } catch (IOException e)
        {
            Log.e("Shaderprogram", vsPath + " nicht gefunden!");
        }

        try
        {
            fs = loadText(assets, fsPath);
        }
        catch(IOException e)
        {
            Log.e("Shaderprogram", fsPath + " nicht gefunden!");
        }

        int vsID, fsID;

        vsID = createShader(GL_VERTEX_SHADER, vs);
        fsID = createShader(GL_FRAGMENT_SHADER, fs);


        program = glCreateProgram();

        if(program != 0)
        {
            glAttachShader(program, vsID);
            glAttachShader(program, fsID);
            glLinkProgram(program);

            int status[] = new int[1];

            glGetProgramiv(program, GL_LINK_STATUS, status, 0);

            if(status[0] != GL_TRUE)
            {
                Log.e("Shaderprogram", "Could not link program: ");
                Log.e("Shaderprogram", glGetProgramInfoLog(program));
                glDeleteProgram(program);
                program = -1;
            }
        }

        glDeleteShader(vsID);
        glDeleteShader(fsID);
    }
    //    ----------------------------------------------------------------------------------------------


    //    Public Methods
    //    ----------------------------------------------------------------------------------------------
    public void use()
    {
        glUseProgram(program);
    }


    public int id()
    {
        return program;
    }


    public int getUniformLocation(String variable)
    {
        return glGetUniformLocation(program, variable);
    }


    public int getAttributeLocation(String variable)
    {
        return glGetAttribLocation(program, variable);
    }


    public void delete()
    {
        glDeleteProgram(program);
    }
    //    ----------------------------------------------------------------------------------------------
}
